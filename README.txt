======== ABOUT =============
This module works along side the weight module. http://drupal.org/project/weight
It's main purpose is to allow you to have the weights of nodes randomly updated
at set intervals.

========= EXAMPLE ==========
For example all nodes of type x could be shuffled once per week.


========= NOTES =============
You must have cron enabled for this module to work correctly.
